﻿using System;
using Inventory.Persistence;
using System.IO;
using SQLite;
using Xamarin.Forms;
using Inventory.Droid.Persistence;

[assembly: Dependency(typeof(SQLiteDb))]


namespace Inventory.Droid.Persistence
{
    public class SQLiteDb : ISQLiteDb
    {
        public SQLiteAsyncConnection GetConnection()
        {
            var documentsPath = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
            var path = Path.Combine(documentsPath, "MySQLite.db3");

            return new SQLiteAsyncConnection(path);
        }
    }
}