﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SQLite;
using Inventory.Persistence;
using System.Collections.ObjectModel;
using Inventory.Models;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Inventory.ViewModels
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ItemEntryForm : ContentPage
    {
        private SQLiteAsyncConnection _connection;
        private ObservableCollection<Items> _items;

        public ItemEntryForm()
        {
            InitializeComponent();
            _connection = DependencyService.Get<ISQLiteDb>().GetConnection();
        }

        protected override async void OnAppearing()
        {
            await _connection.CreateTableAsync<Items>();
            var item = await _connection.Table<Items>().ToListAsync();
            _items = new ObservableCollection<Items>(item);
            base.OnAppearing();
        }

        async void Button_Clicked(object sender, EventArgs e)
        {
            try
            {
                if (txtName.Text == "" || txtPrice.Text == "" || txtStocks.Text == "")
                {
                    await DisplayAlert("Failed", "Please Fill Up The Forms To Add Items ", "OK");
                }
                else
                {
                    var items = new Items { Name = txtName.Text, Price = Convert.ToInt32(txtPrice.Text), Stocks = Convert.ToInt32(txtStocks.Text) };
                    await _connection.InsertAsync(items);
                    _items.Add(items);
                    await DisplayAlert("Success", "Item Added", "OK");
                }
            }
            catch(SQLiteException ex)
            {
                await DisplayAlert("Failed", ex.ToString(), "OK");
            }
        }
    }
}