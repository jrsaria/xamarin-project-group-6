﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SQLite;
using Inventory.Persistence;
using System.Collections.ObjectModel;
using Inventory.Models;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Inventory.ViewModels
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class UpdateItem : ContentPage
    {
        private SQLiteAsyncConnection _connection;
        private ObservableCollection<Items> _items;


        public UpdateItem()
        {
            InitializeComponent();
            _connection = DependencyService.Get<ISQLiteDb>().GetConnection();
        }

        protected override async void OnAppearing()
        {
            await _connection.CreateTableAsync<Items>();
            var item = await _connection.Table<Items>().ToListAsync();
            _items = new ObservableCollection<Items>(item);
            base.OnAppearing();
        }

        async void Button_Clicked(object sender, EventArgs e)
        {
            try
            {
                await _connection.QueryAsync<Items>("Update Items SET Price = '" + txtPrice.Text + "' , Stocks = '" + txtStocks.Text + "' Where Name = '" + txtName.Text + "'");
                await DisplayAlert("Success", "Item Updated", "OK");
            }
            catch (SQLiteException ex)
            {
                await DisplayAlert("Failed", ex.ToString(), "OK");
            }
        }

         void txtName_TextChanged(object sender, TextChangedEventArgs e)
        {
            var price = _connection.QueryAsync<int>("Select Price From Items Where Name = '" + txtName.Text + "'");
            var stocks = _connection.QueryAsync<int>("Select Stocks From Items Where Name = '" + txtName.Text + "'");
            txtPrice.Text = price.ToString();
            txtStocks.Text = stocks.ToString();
        }
    }
}