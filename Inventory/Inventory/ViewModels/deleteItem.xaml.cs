﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SQLite;
using Inventory.Persistence;
using System.Collections.ObjectModel;
using Inventory.Models;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Inventory.ViewModels
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class deleteItem : ContentPage
    {
        private SQLiteAsyncConnection _connection;
        private ObservableCollection<Items> _items;

        public deleteItem()
        {
            InitializeComponent();
            _connection = DependencyService.Get<ISQLiteDb>().GetConnection();
        }

        protected override async void OnAppearing()
        {
            await _connection.CreateTableAsync<Items>();
            var item = await _connection.Table<Items>().ToListAsync();
            _items = new ObservableCollection<Items>(item);
            base.OnAppearing();
        }

        async void Button_Clicked(object sender, EventArgs e)
        {
            try
            {
                await _connection.QueryAsync<Items>("Delete From Items Where Name = '" + txtName.Text + "'");
                await DisplayAlert("Success", "Item Deleted", "OK");
            }
            catch (SQLiteException ex)
            {
                await DisplayAlert("Failed", ex.ToString(), "OK");
            }

        }
    }
}