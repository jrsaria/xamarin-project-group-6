﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using SQLite;
using Inventory.Persistence;
using System.Collections.ObjectModel;
using Inventory.Models;
namespace Inventory
{




    public partial class MainPage : ContentPage
    {
        private SQLiteAsyncConnection _connection;
        private ObservableCollection<Items> _items;
        
        public MainPage()
        {
            InitializeComponent();
            _connection = DependencyService.Get<ISQLiteDb>().GetConnection();

        }

        protected override async void OnAppearing()
        {
           await _connection.CreateTableAsync<Items>();
           var item = await _connection.Table<Items>().ToListAsync();
            _items = new ObservableCollection<Items>(item);
            itemListView.ItemsSource = _items;
            base.OnAppearing();
        }

        async void Button_Clicked(object sender, EventArgs e)
        {
            var items = new Items { Name = "Lapuk" };
            await _connection.InsertAsync(items);
           _items.Add(items);
        }
        /*
        async void Button_Clicked_1(object sender, EventArgs e)
        {
            var items = _items[0];
            items.Name += "UPDATED";
            await _connection.UpdateAsync(items);
        }

        async void Button_Clicked_2(object sender, EventArgs e)
        {
            var lapuk = _items[0];
            await _connection.DeleteAsync(lapuk);
            _items.Remove(lapuk);
        }
        */
        

        
    }
}
