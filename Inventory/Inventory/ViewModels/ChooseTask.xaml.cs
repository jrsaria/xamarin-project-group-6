﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Inventory.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Inventory.ViewModels
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ChooseTask : ContentPage
    {
        public ChooseTask()
        {
            InitializeComponent();
        }

        async void btnView_Clicked(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new MainPage());
        }

        async void btnUpdate_Clicked(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new UpdateItem());

        }

        async void btnAdd_Clicked(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new ItemEntryForm());
        }

        async void btnDelete_Clicked(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new deleteItem()); 
        }
    }
}