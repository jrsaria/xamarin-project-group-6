﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SQLite;
namespace Inventory.Models
{
    public class Items
    {
        [PrimaryKey, AutoIncrement]
        public  int ItemId { get; set; }
        public string Name { get; set; }
        public  double Price { get; set; }
        public  int Stocks { get; set; }
        
    }
}
