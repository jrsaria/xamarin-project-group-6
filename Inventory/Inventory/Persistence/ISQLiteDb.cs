﻿using SQLite;

namespace Inventory.Persistence
{
   public interface ISQLiteDb
    {
        SQLiteAsyncConnection GetConnection();
    }
}
